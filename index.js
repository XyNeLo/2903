const express = require('express');
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.json); //via body-parser
app.use(bodyParser.urlencoded({extended:true}))
const port = 8000;

//habilitar servidor 
app.listen(port,()=>{
    console.log("Projeto executando na porta " + port);
});

//recurso de consulta
app.get('/funcionario',(req, res) =>{
    console.log("Acessando o recurso funcionario");
    res.send("{message:/get funcionario}");
});

app.get('/pesquisa',(req, res) =>{
    let dados = req.query;
    res.send(dados.nome + " " + dados.sobrenome);
});


//via paranmetro
app.get('/pesquisa/cliente/:codigo',(req, res)=>{
    let id = req.params.codigo;
    //console.log(req.params);
    res.send("Dados do Cliente " + id);
});

app.post('funcionario/gravar',(req, res)=>{
    let valores = req.bady;
    console.log("Nome" + valores.nome);
    console.log("Sobrenome" + valores.sobrenome);
    console.log("Idade" + valores.idade);
    res.send("Sucesso")
});